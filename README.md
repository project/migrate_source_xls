CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Usage
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

A source migrate plugin, to allow us to use XLS and XLSX files as a migration
source.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/migrate_source_xls

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/migrate_source_xls


REQUIREMENTS
------------

This module requires the following outside of Drupal core:

 * PHPExcel library


INSTALLATION
------------

To use this plugin you have to:

 * Install PHPExcel library via Composer
 * Install this module via default installation process

 * Install the Migrate source XLS module as you would normally install a
   contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


USAGE
-----

Specify a source plugin in your migration
and all it's params:

```yml
source:
  # Specify the plugin ID.
  plugin: xls
  # Define the path to the XLS source file.
  path: public://mySources/source.xls
  # Define unique key(s).
  keys:
    - id
  # Header row(to figure out which row is should be parsed as a column titles/headers).
  header_row: 1
  # Optional param to prevent parser watching for a non-existent rows(optional).
  offset: 14
  # Optional param to specify the active sheet name.
  sheet_name: 'my_sheet'
```
While you'll specify a fields mapping in your migration
you should keep in mind that your column names will be converted to a machine
readable names, e.g. "My super title" will be "my_super_title".


MAINTAINERS
-----------

 * Valentine Matsveiko (Valentine94) - https://www.drupal.org/u/valentine94

Supporting organizations:

 * I-Next Ltd. - https://www.drupal.org/i-next-ltd
